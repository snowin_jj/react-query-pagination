// https://jsonplaceholder.typicode.com/posts

export type Product = {
  id: number;
  title: string;
  body: string;
};
