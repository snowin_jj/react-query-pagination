import { type FC } from 'react';
import ProductPage from './pages/Product';
import Header from './components/Header';

interface AppProps {}

const App: FC<AppProps> = ({}) => {
  return (
    <main className="container">
      <Header />
      <div className="px-6">
        <ProductPage />
      </div>
    </main>
  );
};
export default App;
