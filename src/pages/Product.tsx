import { useState, type FC } from 'react';
import { useQuery } from '@tanstack/react-query';
import { getProducts } from '../utils/api';
import ProductList from '../components/ProductList';
import { ITEM_PER_PAGE } from '../utils/constants';
import Loader from '../components/Loader';

interface ProductProps {}

const ProductPage: FC<ProductProps> = ({}) => {
  const [page, setPage] = useState<number>(1);

  const { data, isLoading, isFetching } = useQuery({
    queryKey: ['products', page],
    queryFn: () => getProducts(page),
    keepPreviousData: true,
    refetchOnWindowFocus: false,
  });

  if (isLoading || !data || isFetching) return <Loader />;

  return (
    <div className="flex flex-col items-center gap-4 my-8">
      <h1 className="text-4xl font-bold ">Products</h1>
      <ProductList products={data.products} />
      <div className="join">
        <button
          className={`join-item btn `}
          disabled={page === 1}
          onClick={() => setPage((prev) => (prev > 1 ? prev - 1 : prev))}
        >
          «
        </button>
        <button className="join-item btn">Page {page}</button>
        <button
          className="join-item btn"
          disabled={page === Math.ceil(data.totalData / ITEM_PER_PAGE)}
          onClick={() => setPage((prev) => prev + 1)}
        >
          »
        </button>
      </div>
    </div>
  );
};
export default ProductPage;
