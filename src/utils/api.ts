import { Product } from '../types';
import { ITEM_PER_PAGE } from './constants';

export const getProducts = async (page: number) => {
  const response = await fetch(
    `https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=${ITEM_PER_PAGE}`
  );
  const totalData = response.headers.get('X-Total-Count');
  const data = await response.json();
  return {
    products: (data as Product[]) || [],
    totalData: (totalData as unknown as number) || 0,
  };
};
