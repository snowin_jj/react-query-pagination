import { type FC } from 'react';
import type { Product } from '../types';

interface ProductListProps {
  products: Product[];
}

const ProductList: FC<ProductListProps> = ({ products }) => {
  return (
    <div className="my-6 grid grid-cols-3 gap-12">
      {products.map((product) => (
        <div
          key={product.id}
          className="card w-[22rem] h-[24rem] bg-base-100 shadow-xl"
        >
          <figure>
            <img
              src="https://picsum.photos/400"
              alt="random image"
              className="w-full h-full block object-cover"
            />
          </figure>
          <div className="card-body">
            <h2 className="card-title">
              {product.title.split(' ').slice(0, 5).join(' ')}
              <div className="badge badge-secondary">NEW</div>
            </h2>
            <p>{product.body.split(' ').slice(0, 10).join(' ')}</p>
            <div className="card-actions justify-end">
              <a href="/">Read more</a>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};
export default ProductList;
