import { type FC } from 'react';

interface LoaderProps {}

const Loader: FC<LoaderProps> = ({}) => {
  return (
    <div className="h-screen -mt-16 grid place-items-center">
      <div className="flex flex-col items-center gap-2">
        <span className="loading loading-dots loading-lg"></span>
        <p className="text-xl">loading</p>
      </div>
    </div>
  );
};
export default Loader;
